#Derek Li
#ShapeDrwaing
#November 11th 2016

import turtle as T


def StarShape(speed, color): #this is a function of a shape

    T.speed(speed)
    T.pencolor(color)
    T.circle(10)
    T.forward(100)
    T.right(150)
    T.forward(120)
    T.right(150)
    T.forward(100)
    T.right(120)
    T.forward(100)
    T.right(30)
    T.right(120)
    T.forward(120)

def drawStar(): #this is a function of a shape
    T.Screen()
    StarShape(2,"red")
    StarShape(2,"green")
    StarShape(2,"purple")
    StarShape(2,"black")
    StarShape(2,"grey")
    StarShape(2,"purple")
    StarShape(2,"red")
    StarShape(2,"green")
    StarShape(2,"purple")
    StarShape(2,"black")
    StarShape(2,"grey")
    StarShape(2,"purple")

    T.done()


def newFunc(): #this is a function of a shape
    spiral = T.Turtle()
    for i in range(15):
        spiral.forward(i * 10)
        spiral.right(100)

    T.done()



def ultimatefunc(): #this is a function of a shape
    painter = T.Turtle()

    painter.pencolor("blue")

    for i in range(50):
        painter.forward(50)
        painter.left(123)

    painter.pencolor("red")
    for i in range(50):
        painter.forward(100)
        painter.left(123)

    T.done()


def interestingfunc(): #this is a function of a shape

    ninja = T.Turtle()

    ninja.speed(20)

    for i in range(150):
        ninja.forward(90)
        ninja.right(25)
        ninja.forward(30)
        ninja.left(45)
        ninja.forward(30)
        ninja.right(40)
        ninja.penup()
        ninja.setposition(0, 0)
        ninja.pendown()
        ninja.right(2)

    T.done()

print("what do you want to draw?") #Giving instructions of options
print("Options: star, surprise,fun or interesting")

userinput = input().lower()

print("what color do you want?") #Giving instructions of colors

colorInput = input().lower()

print("What size do you want to draw?") #Giving instructions of color
print("Small? Medium? or Large?")

sizeInput = input().lower() #Size userinput
small = 50 #sieze
medium = 100 #size
large = 150 #size


if userinput == "star": # Userinput-Star Function
    if sizeInput == "small":
        small = 50;
    elif sizeInput == "medium":
        meidum = 100;
    elif sizeInput == "large";
        large = 150;
    drawStar(colorInput, sizeInput)

elif userinput == "surprise": # Userinput-Surprise Function
    if sizeInput == "small":
        small = 50;
    elif sizeInput == "medium":
        meidum = 100;
    elif sizeInput == "large";
        large = 150;
    newFunc(colorInput, sizeInput)

elif userinput == "fun": # Userinput-Fun Function
    if sizeInput == "small":
        small = 50;
    elif sizeInput == "medium":
        meidum = 100;
    elif sizeInput == "large";
        large = 150;
    ultimatefunc(colorInput, sizeInput)

elif userinput == "interesting": # Userinput-interesting Function
    if sizeInput == "small":
        small = 50;
    elif sizeInput == "medium":
        meidum = 100;
    elif sizeInput == "large";
        large = 150;
    interestingfunc(colorInput, sizeInput)

else: # function of wrong command
    print("You did not enter a valid option, Sorry!")

